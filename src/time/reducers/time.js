import {
    ADD_TICK,
    CHANGE_CURRENT_TICK
} from '../actions/time';

const initialState = {
    ticks: [],
    writingTick: '',
    runningTick: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TICK:
            return {
                ...state,
                ticks: state.ticks.concat([action.payload]),
                runningTick: state.ticks.length
            };
        case CHANGE_CURRENT_TICK:
            return {
                ...state,
                writingTick: action.payload
            };
        default:
            return state;
    }
}

