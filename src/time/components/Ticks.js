import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Ticks extends Component {
    render() {
        const { ticks, tick, runningTick, addTick, changeCurrentTick } = this.props;

        return (
            <div className="App">
                { JSON.stringify(runningTick) }
                <input type="text" value={tick} onChange={ e => changeCurrentTick(e.target.value)  } />
                <button onClick={ () => addTick(tick) }>OK</button>

                { ticks.map(tick => (
                    <div>{ JSON.stringify(tick) }</div>
                )) }

            </div>
        );
    }
}

Ticks.propTypes = {
    ticks: PropTypes.array,
    addTick: PropTypes.func,
    tick: PropTypes.string,
    changeCurrentTick: PropTypes.func,
    runningTick: PropTypes.Object
};

export default Ticks;
