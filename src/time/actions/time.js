export const ADD_TICK = 'time/ADD_TICK';
export const CHANGE_CURRENT_TICK = 'time/CHANGE_CURRENT_TICK';
export const SET_RUNNING_TICK = 'time/SET_RUNNING_TICK';

/**
 * Adds new time tick.
 */
export function addTick (tick) {
    return {
        type: ADD_TICK,
        payload: tick,
    };
}

/**
 * Updates current writing tick.
 */
export function changeCurrentTick (text) {
    return {
        type: CHANGE_CURRENT_TICK,
        payload: text,
    }
}
