import { connect } from 'react-redux';
import Ticks from '../components/Ticks';
import { addTick, changeCurrentTick } from '../actions/time';

const stringifyTick = tick => {
    return `${tick.task || ''}${null !== tick.tag ? `@${tick.tag}` : ''}`;
};

const parseTickQuery = text => {
    const data = text.split('@');

    return {
        task: data[0] || null,
        tag: undefined !== data[1] ? data[1] : null,
        start: Date.now()
    };
};

const mapStateToProps = ({time}) => ({
    ticks: time.ticks,
    tick: time.writingTick,
    runningTick: time.ticks[time.runningTick]
});

const mapDispatchToProps = dispatch => ({
    addTick: text => dispatch(addTick(parseTickQuery(text))),
    changeCurrentTick: text => dispatch(changeCurrentTick(text)),
});

const VisibleTicks = connect(mapStateToProps, mapDispatchToProps)(Ticks);

export default VisibleTicks;
