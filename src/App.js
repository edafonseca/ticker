import React, { Component } from 'react';
import './App.css';
import VisibleTicks from './time/containers/VisibleTicks';

class App extends Component {
  render() {
    return (
      <div className="App">
        <VisibleTicks />
      </div>
    );
  }
}

export default App;
