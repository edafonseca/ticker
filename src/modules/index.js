import { combineReducers } from 'redux'
import time from '../time/reducers/time';

export default combineReducers({
    time,
})
